// import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'dart:math';
import 'package:flutter/cupertino.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  // Colors:
  Color _mainAppBarColor = CupertinoColors.label;
  Color _mainBodyBackgroundColor = CupertinoColors.systemBackground;
  Color _grayBackgroundColor = CupertinoColors.extraLightBackgroundGray;
  Color _inconspicuousIconButton = CupertinoColors.lightBackgroundGray;
  // Color _grayFontColor = CupertinoColors.lightBackgroundGray;
  // Color _blackFontColor = CupertinoColors.label;
  Color _whiteFontColor = CupertinoColors.systemBackground;

  // Used for animations:
  double _setButtonOpacity = 0.0;

  // Bool variables:
  bool _nameInTextField = false;

  // TextFields input:
  final _enterNameTextField = TextEditingController();

  // Constant Lists
  final _iconsList = [
    "assets/x0.png",
    "assets/x1.png",
    "assets/x2.png",
    "assets/x3.png",
    "assets/x4.png",
    "assets/x5.png",
    "assets/x6.png",
    "assets/x7.png",
    "assets/x8.png",
    "assets/x9.png",
    "assets/x10.png",
    "assets/x11.png",
    "assets/x12.png",
    "assets/x13.png",
    "assets/x14.png",
    "assets/x15.png",
    "assets/x16.png",
    "assets/x17.png",
    "assets/x18.png",
    "assets/x19.png",
    "assets/x20.png",
    "assets/x21.png",
    "assets/x22.png",
    "assets/x23.png",
    "assets/x24.png",
    "assets/x25.png",
    "assets/x26.png",
    "assets/x27.png",
    "assets/x28.png",
    "assets/x29.png",
    "assets/x30.png",
    "assets/x31.png",
    "assets/x32.png",
  ];

  // Variable Lists
  var personList = List<Widget>();

  String name = 'not set';
  final savedName = Set<String>();
  final savedIcon = Set<Image>();
  static bool _setButtonVisible = false;

  @override
  // build:
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: setNamePageAppBar(),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text(
                'Name Saver',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Huggable',
                  fontSize: 28,
                  color: _whiteFontColor,
                ),
              ),
              decoration: BoxDecoration(
                color: _mainAppBarColor,
              ),
            ),
            /* History */ ListTile(
              trailing: Icon(CupertinoIcons.clock),
              title: Text('History'),
              onTap: () {
                buildHistoryPage();
              },
            ),
            /* Settings */ ListTile(
              trailing: Icon(CupertinoIcons.gear),
              title: Text('Settings'),
              onTap: () {
                buildSettingsPage();
              },
            ),
          ],
        ),
      ),
      body: setNamePageBody(),
    );
  }

  // 'set name' page AppBar
  Widget setNamePageAppBar() {
    return AppBar(
      backgroundColor: _mainAppBarColor,
      title: Text('Home'),
      actions: [
        IconButton(
          icon: Icon(CupertinoIcons.info),
          color: _inconspicuousIconButton,
          onPressed: buildInformationPage,
        )
      ],
    );
  }

  // 'set name' page Body
  Widget setNamePageBody() {
    return Container(
      color: _mainBodyBackgroundColor,
      padding: EdgeInsets.all(30),
      // end of main configuration
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        // end of main configuration
        children: [
          /* SizedBox */ SizedBox(
            height: 30,
          ),
          /* 'Your name is' Text' */ Container(
            child: AutoSizeText(
              'Your name is:',
              style: TextStyle(fontSize: 20),
              maxLines: 1,
            ),
          ),
          /* Name Text */ Container(
            child: AutoSizeText(
              '$name',
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                height: 1.5,
              ),
              maxLines: 1,
            ),
          ),
          /* SizedBox */ SizedBox(
            height: 20,
          ),
          /* Text Field */ Container(
            child: CupertinoTextField(
              onChanged: (textInside) {
                somethingInEnterName(textInside);
              },
              keyboardType: TextInputType.name,
              controller: _enterNameTextField,
              textAlign: TextAlign.center,
              maxLength: 40,
              maxLines: 1,
              autocorrect: false,
              decoration: BoxDecoration(
                color: CupertinoColors.lightBackgroundGray,
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
              ),
              placeholder: "Enter you name",
              placeholderStyle: TextStyle(color: CupertinoColors.systemGrey2),
            ),
          ),
          /* SizedBox */ SizedBox(
            height: 20,
          ),
          /* Button */ Container(
            child: SizedBox(
              width: 330,
              height: 30,
              child: AnimatedOpacity(
                onEnd: () {
                  if (_nameInTextField == true) {
                    _setButtonVisible = true;
                  } else {
                    _setButtonVisible = false;
                  }
                },
                duration: Duration(milliseconds: 300),
                opacity: _setButtonOpacity,
                child: Visibility(
                  visible: _setButtonVisible,
                  child: CupertinoButton(
                    pressedOpacity: 0.8,
                    padding: EdgeInsets.all(0.0),
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    child: (Text(
                      'set',
                      style: TextStyle(
                        fontSize: 17,
                      ),
                    )),
                    onPressed: () {
                      setState(
                        () {
                          // make new variables
                          String _enteredName;
                          AssetImage _randomIcon;

                          // set the variables
                          _enteredName = _enterNameTextField.text;
                          _randomIcon = getRandomIcon();

                          // create a new person with the values of the variables
                          personList.insert(
                            personList.length,
                            person(
                                _randomIcon, _enteredName, personList.length),
                          );

                          // query there something in the setName texfield
                          somethingInEnterName(_enterNameTextField.text);

                          // clear textflied and fade button out
                          _enterNameTextField.clear();
                          animationSetButtonZurueck();
                        },
                      );
                    },
                    color: Colors.green,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // build 'all names' page:
  void buildHistoryPage() {
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) {
        return Scaffold(
          appBar: historyPageAppBar(),
          body: historyPageBody(),
        );
      },
    ));
  }

  // 'all names' page AppBar
  Widget historyPageAppBar() {
    return AppBar(
      backgroundColor: _mainAppBarColor,
      title: Text('History'),
      actions: [
        IconButton(
          icon: Icon(CupertinoIcons.delete),
          onPressed: () {
            personList.clear();
            buildHistoryPage();
          },
        )
      ],
    );
  }

  // 'all names' page Body
  /*
  Widget allNamesPageBody(context) {
    final divided = ListTile.divideTiles(
      context: context,
      tiles: tiles,
    ).toList().reversed.toList();

    return Scaffold(
      body: ListView(
        children: divided,
      ),
      backgroundColor: _mainBodyBackgroundColor,
    );
  }
  */

  Widget historyPageBody() {
    return ListView.builder(
      itemCount: personList.length,
      itemBuilder: (_, index) {
        return Card(
          color: Colors.black,
          child: Card(
            child: personList.reversed.elementAt(index),
            borderOnForeground: false,
          ),
        );
      },
    );
  }

  // build 'information' page:
  void buildInformationPage() {
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) {
        return Scaffold(
          appBar: informationPageAppBar(),
          body: informationPageBody(),
        );
      },
    ));
  }

  // 'information' page AppBar
  Widget informationPageAppBar() {
    return AppBar(
      backgroundColor: _mainAppBarColor,
      title: Text('Information'),
    );
  }

  // 'information' page Body
  Widget informationPageBody() {
    return Container(
      color: _mainBodyBackgroundColor,
      padding: EdgeInsets.all(30),
      alignment: Alignment(0.0, 0.0),
      // end of main configuration
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        // end of main configuration
        children: [
          /* by Text */ Container(
            child: Text(
              'Name Saver App',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 35,
                fontFamily: 'Huggable',
              ),
            ),
          ),
          /* SizedBox */ SizedBox(
            height: 5,
          ),
          /* by Text Jorit */ Container(
            child: Text(
              'Ⓒby: Jorit Vásconez Gerlach',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15,
              ),
            ),
          ),
          /* by Text Fynn */ Container(
            child: Text(
              'and Fynn Frings',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 13,
              ),
            ),
          ),
        ],
      ),
    );
  }

  // build 'settings' page:
  void buildSettingsPage() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return Scaffold(
            appBar: settingsPageAppBar(),
            body: settingsPageBody(),
          );
        },
      ),
    );
  }

  // 'settings' page AppBar
  Widget settingsPageAppBar() {
    return AppBar(
      backgroundColor: _mainAppBarColor,
      title: Text('Settings'),
    );
  }

  // 'settings' page Body
  Widget settingsPageBody() {
    return Container(
      child: Text(
        'coming soon...',
        style: TextStyle(color: Colors.black),
      ),
    );
  }

  // Widget 'Person'
  Widget person(AssetImage icon, String name, int id) {
    return ListTile(
      leading: Container(
        height: 45.0,
        width: 45.0,
        // end of main configuration
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          // end of main configuration
          image: DecorationImage(
            image: icon,
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
      title: AutoSizeText(name, maxLines: 1),
      subtitle: Text("just a person"),
      trailing: IconButton(
        icon: Icon(
          CupertinoIcons.multiply,
          size: 30,
        ),
        onPressed: () {
          setState(
            () {
              personList.removeAt(id);
              (context as Element).reassemble();
              /* -SnackBar additional feature-
                ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  backgroundColor: Colors.black,
                  duration: const Duration(seconds: 2),
                  content: Text('$name wurde entfernt'),
                  action: SnackBarAction(
                    label: 'Undo',
                    onPressed: () {
                      savedName.add(name);
                      (context as Element).reassemble();
                    },
                  ),
                ),
              );
              */
            },
          );
        },
      ),
    );
  }

  /*-----Methods----*/

  // query if there is something ib the 'enterName' textfield
  void somethingInEnterName(String textInside) {
    if (textInside.isEmpty) {
      animationSetButtonZurueck();
      _nameInTextField = false;
    } else {
      animationSetButtonHin();
      _nameInTextField = true;
    }
  }

  // ge
  AssetImage getRandomIcon() {
    return AssetImage(_iconsList[Random().nextInt(_iconsList.length)]);
  }

  void animationSetButtonHin() {
    setState(() {
      _setButtonOpacity = 1.0;
      _setButtonVisible = true;
    });
  }

  void animationSetButtonZurueck() {
    setState(() {
      _setButtonOpacity = 0.0;
    });
  }
}
